using UnityEngine;
using System.Collections;
using System;

public class Mover : MonoBehaviour {

    // define type of movement
    public enum motionType { Simple, Complex };

    // define the possible states through an enumeration
    public enum motionDirections { Spin, Horizontal, Vertical };

    // store the type
    public motionType type = motionType.Simple;

    // store the state
    public motionDirections motionState = motionDirections.Horizontal;

    // motion parameters
    public float spinSpeed = 180.0f;
    public float motionMagnitude = 0.1f;
    public float timeBetweenVelocityChanges = 0.5f;
    public Vector3 initialVelocity = new Vector3();

    // current position of object
    [HideInInspector]
    public Vector3 currentPosition = new Vector3();

    // current rotation of an object
    [HideInInspector]
    public Quaternion currentRotation = new Quaternion();

    // creates a box for the object to move in
    public SpatialConfig spaceCreator = new SpatialConfig();

    // used to create random linear velocities 
    public SpatialConfig velocityCreator = new SpatialConfig();

    // current angular or linear velocity
    private Vector3 velocity = new Vector3();

    // time to change movement direction
    private float timeChangedirection = 0.0f;

    // last position and rotation of object
    private Vector3 lastPosition = new Vector3();
    private Quaternion lastRotation = new Quaternion();

    // initializes some variables
    void Start()
    {
        // record initial position and rotation of object
        currentPosition = this.transform.localPosition;
        currentRotation = this.transform.localRotation;
        lastPosition = currentPosition;
        lastRotation = currentRotation;

        // sets the velocity as the initialVelocity
        velocity = initialVelocity;

        // initializes the time to change direction
        timeChangedirection = Time.time + timeBetweenVelocityChanges;
    }

	// Update is called once per frame
	void Update ()
    {
        // sets last position and rotation 
        lastPosition = this.transform.localPosition;
        lastRotation = this.transform.localRotation;

        // selects which motion function to use
	    switch (type)
        {
            case motionType.Simple:
                SimpleMover();
                break;
            case motionType.Complex:
                ComplexMover();
                break;
        }

        // records current position and rotation of object
        currentPosition = this.transform.localPosition;
        currentRotation = this.transform.localRotation;
    }

    // simple movement system
    void SimpleMover()
    {
        // do the appropriate motion based on the motionState
        switch (motionState)
        {
            case motionDirections.Spin:
                // rotate around the up axix of the gameObject
                gameObject.transform.Rotate(Vector3.up * spinSpeed * Time.deltaTime);
                break;
            case motionDirections.Horizontal:
                // move up and down over time
                gameObject.transform.Translate(Vector3.right * Mathf.Cos(Time.timeSinceLevelLoad) * motionMagnitude);
                break;
            case motionDirections.Vertical:
                // move up and down over time
                gameObject.transform.Translate(Vector3.up * Mathf.Cos(Time.timeSinceLevelLoad) * motionMagnitude);
                break;
        }
    }

    // complex movement system
    void ComplexMover()
    {
        GameObject daddy = this.transform.parent.gameObject;

        // change the direction in the correct times
        if (Time.time >= timeChangedirection)
        {
            // determines the type of velocity 
            switch (velocityCreator.config)
            {
                case SpatialConfig.SelectConfig.box:
                    velocity = velocityCreator.BoxSpace(); // linear velocity vector
                    break;
                case SpatialConfig.SelectConfig.sphere:
                    velocity = velocityCreator.SphereSpace(); // angular velocity vector
                    break;
            }

            // set new time to change direction
            timeChangedirection = Time.time + timeBetweenVelocityChanges;
        }

        // move the object using the velocity created
        switch (velocityCreator.config)
        {
            case SpatialConfig.SelectConfig.box:
                transform.Translate(velocity * Time.deltaTime, daddy.transform);
                break;
            case SpatialConfig.SelectConfig.sphere:
                transform.RotateAround(daddy.transform.position, velocity, (float)Math.Sqrt(Vector3.Dot(velocity, velocity)) * Time.deltaTime);
                break;
        }

        // checks to see if the movement lead the object out of the box
        if (OutOfBounds(currentPosition))
        {
            // will avoid moving object out of the box
            currentPosition = lastPosition;
            currentRotation = lastRotation;

            // place the object back into de box
            this.transform.localPosition = currentPosition;
            this.transform.localRotation = currentRotation;

            // set the velocity to the initial one
            velocity = initialVelocity;
        }
                
    }

    // determines if position is within the bounds of a spherical or cubic box
    bool OutOfBounds(Vector3 pos)
    {
        // local variables
        Vector3 convertedPos = ConvertToSpherical(pos);
        bool condition = true; // condition to be tested
        bool isInside;

        // selects which box the object is moving in
        // create condition to be tested based on the boundaries of the selected box
        switch (spaceCreator.config)
        {
            case SpatialConfig.SelectConfig.box:
                condition = pos.x >= spaceCreator.xMinRange && pos.x <= spaceCreator.xMaxRange;
                condition = condition && pos.y >= spaceCreator.yMinRange && pos.y <= spaceCreator.yMaxRange;
                condition = condition && pos.z >= spaceCreator.zMinRange && pos.z <= spaceCreator.zMaxRange;
                break;
            case SpatialConfig.SelectConfig.sphere:
                condition = convertedPos.x >= spaceCreator.minRadius && convertedPos.x <= spaceCreator.maxRadius;
                condition = condition && convertedPos.y >= spaceCreator.minAzimuth && convertedPos.y <= spaceCreator.maxAzimuth;
                condition = condition && convertedPos.z >= spaceCreator.minPolar && convertedPos.z <= spaceCreator.maxPolar;
                break;
        }

        if (condition)
        {
            isInside = true;
        } else
        {
            isInside = false;
        }

        return isInside;
    }

    // converts a vector in cartesian coordnates to spherical coordnates
    Vector3 ConvertToSpherical(Vector3 vec)
    {
        // local variables
        Vector3 coords = new Vector3();

        // spherical coordnate system is (radius, azimuth, polar)
        // x => radius; y => azimuth; z => polar
        coords.x = (float)Math.Sqrt(Vector3.Dot(vec, vec));
        coords.z = (float)Math.Acos(vec.y / coords.x);
        coords.y = (float)Math.Acos(vec.x / (coords.x * (float)Math.Sin(coords.z)));

        return coords;
    }
}
